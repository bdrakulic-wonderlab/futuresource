public with sharing class APIUtilities {

	public static final String authEndPoint = 'https://data.futuresource-consulting.com/api/v1/authenticate?username=annabelle.normington%40nimbuspoint.com&password=n1mbuspoint1';
	public static final String accsEndPoint = 'https://data.futuresource-consulting.com/api/v1/accounts';
	public static final String subscribersEndPoint = 'https://data.futuresource-consulting.com/api/v1/subscribers';
	public static final String username = 'annabelle.normington@nimbuspoint.com:';
	
	public static HttpResponse sendHttpReq(String url, Map<String,String> params, String callMethod){	
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(url);
		request.setMethod(callMethod);
		String auth;
		String requestBody;
		if(params != null ){
			auth = params.get('auth');
			requestBody = params.get('requestBody');
		} 
		if(!String.isBlank(auth)){
			request.setHeader('Authorization','Basic ' +  auth);
		}
		if(!String.isBlank(requestBody)){
			request.setHeader('Content-Type', 'application/json;charset=UTF-8');
			request.setBody(requestBody);
		}
		System.debug('REQUEST: ' + request);
		HttpResponse response = http.send(request);
		return response;
	}

	public static HttpResponse getSecurityToken(){
		HttpResponse response = sendHttpReq(authEndPoint, null, 'GET');
		return response;
	}
}