public class AccountTriggerHandler {

	public static void syncAccountsWithFS(List<Account> accounts){
		AccountCalloutHandler accHandler = new AccountCalloutHandler(Trigger.new);
		Id jobID = System.enqueueJob(accHandler);
		System.debug('JobId: ' + jobID);
	}
}