public with sharing class PIContactsCalloutHandler implements Queueable {

	private List<Publisher_Interactive__c> piContacts;
	
	public PIContactsCalloutHandler() {
	}
	
	public PIContactsCalloutHandler(List<Publisher_Interactive__c> piContacts) {
		this.piContacts = piContacts;
	}

	public void execute(QueueableContext context) {
		processPISubscriberSync();
	}

	@Future(callout=true)
	public static void process(Set<String> piIds){
		List<Publisher_Interactive__c> piCons = [SELECT Id, 
														isEnabled__c, 
														accountName__c, 
														Username__c,
														Name,
														Contact__r.Email,
														Contact__r.Phone,
														Contact__r.Title,
														Contact__r.Department
															FROM Publisher_Interactive__c WHERE Id IN :piIds];
		HttpResponse authResponse = APIUtilities.getSecurityToken();
		System.debug('AUTH RESPONSE: ' + authResponse);
		String securityToken = authResponse.getBody();
		String auth = String.format('{0}{1}', new String[]{APIUtilities.username,securityToken.remove('"')});
		String authBase64String = EncodingUtil.base64Encode(Blob.valueof(auth));
		
		if(authResponse != null && authResponse.getStatusCode() == 200){
			Integer statusCode = checkIfPIExists(authBase64String, piCons);
			String actionType = statusCode != 200 ? 'CREATE' : 'UPDATE';
			//sync PIContacts
			syncPIContacts(authBase64String, piCons, actionType);
		}
	}

	private void processPISubscriberSync(){
		Set<String> piIds = new Set<String>();
		
		for(Publisher_Interactive__c pi: this.piContacts){
			piIds.add(pi.Id);
		}
		process(piIds);
	}

	private static Integer checkIfPIExists(String authBase64String, List<Publisher_Interactive__c> piCons){
		String userName = piCons[0].Username__c;
		String userNameParam = '/' + userName.replace(' ', '%20');
		String endPoint = APIUtilities.subscribersEndPoint + userNameParam;
		Map<String, String> params = new Map<String, String>{'auth' => authBase64String};
		HttpResponse response = APIUtilities.sendHttpReq(endPoint, params, 'GET');
		System.debug('checkIfPIExists RESPONSE: ' + response);
		return response.getStatusCode();
	}

	private static void syncPIContacts(String authBase64String, List<Publisher_Interactive__c> piCons, String actionType){
		Publisher_Interactive__c pi = piCons[0]; //should it be handled for 25 PI contacts?
		
		String accountNameParam = '/' + pi.accountName__c.replace(' ','%20');
		String userNameParam = '/' + pi.Username__c.replace(' ', '%20');
		Boolean sendWelcomeEmail = actionType == 'CREATE' ? true : false;
		String requestBodyJSON = '{' + 
								'"name": "' + pi.Name +  '",' + 
								'"email": "' + pi.Contact__r.Email +  '",' + 
								'"phone": "' + pi.Contact__r.Phone +  '",' + 
								'"jobTitle":"' + pi.Contact__r.Title +  '",' + 
								'"department": "' + pi.Contact__r.Department +  '",' + 
								'"accountName": "' + pi.accountName__c +  '",' + 
								'"isEnabled": ' + pi.isEnabled__c + ',' + 
								'"company": "Future Source",' + 
								'"countryCode": "GB",' + 
								'"languageCode": "GB",' + 
								'"sendWelcomeEmail":' + sendWelcomeEmail + 
								'}';

		Map<String, String> params = new Map<String, String>{'auth' => authBase64String, 'requestBody' => requestBodyJSON};
		String endpoint = APIUtilities.accsEndPoint + accountNameParam.remove('"') + '/subscribers' + userNameParam.remove('"');
		HttpResponse response = APIUtilities.sendHttpReq(endPoint, params, 'PUT');
		if(response !=null && (response.getStatusCode() == 400 || response.getStatusCode() == 404)){
			System.debug(response.getStatusCode() + '--' + response.getStatus());
		}
	}
	
}