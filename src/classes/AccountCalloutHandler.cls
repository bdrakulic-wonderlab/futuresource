public class AccountCalloutHandler implements Queueable {

	private List<Account> accounts;
	public AccountCalloutHandler(){

	}

	public AccountCalloutHandler(List<Account> accounts){
		this.accounts = accounts;
	}

	public void execute(QueueableContext context) {
		Set<String> accsIds = new Set<String>();
		
		for(Account acc: this.accounts){
			accsIds.add(acc.Id);
		}
		process(accsIds);
	}

	@Future(callout=true)
	public static void process(Set<String> accsIds){
		List<Account> accounts = [SELECT Id, accountName__c FROM Account WHERE Id IN :accsIds];
		HttpResponse authResponse = APIUtilities.getSecurityToken();
		String securityToken = authResponse.getBody();
		String auth = String.format('{0}{1}', new String[]{APIUtilities.username,securityToken.remove('"')});
		String authBase64String = EncodingUtil.base64Encode(Blob.valueof(auth));
		System.debug('AUTH RESPONSE: ' + authResponse);
		
		if(authResponse != null && authResponse.getStatusCode() == 200){
			//sync Accounts
			syncAccountCreation(authBase64String, accounts);
		}
	}
	
	// Create or edit an account - PUT /accounts/{accountName}
	private static void syncAccountCreation(String authBase64String, List<Account> accounts){
		Account acc = accounts[0]; //should account creation be handled for 25 accounts?
		System.debug('accountName__c: ' + acc.accountName__c);
		String accountNameParam = '/' + acc.accountName__c.replace(' ','%20');
		String requestBodyJSON = '{"companyName": "Future Source","countryCode": "GB","isEnabled": true}';
		Map<String, String> params = new Map<String, String>{'auth' => authBase64String, 'requestBody' => requestBodyJSON};
		String endpoint = APIUtilities.accsEndPoint + accountNameParam.remove('"');
		HttpResponse response = APIUtilities.sendHttpReq(endpoint, params, 'PUT');
		if(response !=null && response.getStatusCode() == 400){
			System.debug('ERROR! Status Code: ' + response.getStatusCode() + ', ' + 'Status Message: ' + response.getStatus());	
		}
	}
}