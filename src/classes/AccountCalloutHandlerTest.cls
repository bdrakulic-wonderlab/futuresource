@isTest
public class AccountCalloutHandlerTest {

    @testSetup 
    static void setup() {
        insert new Account(
                Name='Test Account',
                ShippingCountry = 'UK'
            );
        
    }

    @isTest
    static void testQueueable() {
        List<AsyncApexJob> aajs = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob];
		System.assert(aajs.size() > 0);
    }

	@isTest
    static void testFutureCallout(){
		List<Account> accs = [SELECT Id FROM Account];
		Set<String> accsIds = new Set<String>();
		for(Account acc: accs){
			accsIds.add(acc.Id);
		}
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200,'Success!', 'Test response body', null));
		Test.startTest();
		AccountCalloutHandler.process(accsIds);
		Test.stopTest();
	}
}