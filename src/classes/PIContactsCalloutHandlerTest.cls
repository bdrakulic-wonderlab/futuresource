@isTest
private class PIContactsCalloutHandlerTest {
	
	@testSetup 
    static void setup() {
		Account acc = new Account(Name = 'TestAccount', ShippingCountry = 'UK');
		insert acc;

		Contact con = new Contact(FirstName ='Test', 
									LastName = 'Contact', 
									Email ='test@test.test', 
									Title = 'CEO', 
									Department = 'Fruits and Vegetables',
									Phone = '08032432554',
									Username__c = 'TestUserName',
									AccountId = acc.Id
									);
		insert con;

		Publisher_Interactive__c pi = new Publisher_Interactive__c(Name = 'Test PI', isEnabled__c = true, Contact__c = con.Id);
		insert pi;
    }

    @isTest
    static void testQueueable() {
        List<AsyncApexJob> aajs = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob];
		System.assert(aajs.size() > 0);
    }

	@isTest
    static void testFutureCallout(){
		List<Publisher_Interactive__c> piCons = [SELECT Id FROM Publisher_Interactive__c];
		Set<String> piConsIds = new Set<String>();
		for(Publisher_Interactive__c pi: piCons){
			piConsIds.add(pi.Id);
		}
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200,'Success!', 'Test response body', null));
		Test.startTest();
		PIContactsCalloutHandler.process(piConsIds);
		Test.stopTest();
	}

   /* @isTest
    static void testGetSecurityToken(){
        String statusMessage = 'The authentication key to use in future API requests.';
        String testToken = 'TESTTEST';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, statusMessage, testToken, null));
        HttpResponse response = PIContactsCalloutHandler.getSecurityToken();
        Integer statusCode = response.getStatusCode();
        String status = response.getStatus();
        String body = response.getBody();
        System.assert(statusCode == 200);
        System.assert(status == statusMessage);
        System.assert(body == testToken);
    }*/
	
}