public with sharing class PublisherInteractiveTriggerHandler {
	
	public static void syncPIContacts(){
		PIContactsCalloutHandler piHandler = new PIContactsCalloutHandler(Trigger.new);
		Id jobID = System.enqueueJob(piHandler);
		System.debug('JobId: ' + jobID);
	}
}