trigger PublisherInterActiveContactTrigger on Publisher_Interactive__c (
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete) {

         if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) { 
             PublisherInterActiveTriggerHandler.syncPIContacts();
         }
}